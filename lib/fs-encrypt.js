const _ = require( 'lodash' ),
    fse = require( 'fs-extra' ),
    crypto = require( 'crypto' ),
    algorithm = 'aes-256-gcm';

/**
 * @param file
 * @param data
 * @param password - crypto.randomBytes( 16 ).toString( 'hex' )
 * @param callback
 */
function outputJson( file, data, password, callback ) {
    if( !password ) return callback( new Error( 'Can\'t encrypt file ' + file + '. Password is empty' ) );
    const iv = crypto.randomBytes( 12 ),
        cipher = crypto.createCipheriv( algorithm, password, iv );

    var encrypted = cipher.update( JSON.stringify( data ), 'utf8', 'hex' );
    encrypted += cipher.final( 'hex' );

    const encData = {
        iv: iv.toString( 'hex' ),
        tag: cipher.getAuthTag(),
        content: encrypted
    };

    fse.outputJson( file, encData, {spaces: 0}, callback );
}

/**
 * @param file
 * @param password - crypto.randomBytes( 16 ).toString( 'hex' )
 * @param callback
 */

function readJson( file, password, callback ) {
    if( !password ) return callback( new Error( 'Can\'t decrypt file ' + file + '. Password is empty' ) );
    fse.readJson( file, function( err, encData ) {
        if( err ) return callback( err );
        if( _.isEmpty( encData ) ) return callback( new Error( 'Can\'t read key file ' + file ) );
        const decipher = crypto.createDecipheriv( algorithm, password, new Buffer( encData.iv, 'hex' ) );
        decipher.setAuthTag( new Buffer( encData.tag.data ) );
        var data = decipher.update( encData.content, 'hex', 'utf8' );
        try
        {
            data += decipher.final( 'utf8' );
        } catch( err )
        {
            if( err ) return callback( err );
        }
        try
        {
            callback( null, JSON.parse( data ) );
        } catch( err )
        {
            if( err ) return callback( err );
        }
    } );
}

module.exports = {
    outputJson: outputJson,
    readJson: readJson
};