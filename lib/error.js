const _ = require( 'lodash' ),
    httpStatus = {
        400: 'Bad Request',
        401: 'Unauthorized',
        403: 'Forbidden',
        404: 'Not found',
        406: 'Not Acceptable',
        500: 'Internal Server Error'
    },
    errors = {
        0: [500, 'System error'],
        1: [403, 'API key not set'],
        2: [403, 'Invalid API key'],
        3: [403, 'Your current ip address is not whitelisted for that API key'],
        4: [401, 'Authorization header is not set'],
        5: [401, 'Invalid JSON web token'],
        6: [401, 'Invalid JSON web token payload'],
        7: [403, 'Your token has no access to that bot account'],
        10: [400, 'Invalid steamID64 url parameter'],
        11: [400, 'Invalid tradeoffer id parameter'],
        20: [401, 'Steam Guard code required'],
        21: [401, 'Two factor authentication already enabled on that account'],
        26: [401, 'Two factor authentication error'],
        22: [401, 'Two factor authentication activation code required'],
        23: [401, 'Two factor authentication activation failed'],
        24: [401, 'steamID64 is not consistent with the provided credentials'],
        25: [429, 'Two factor authentication activation code required'],
        40: [400, 'Unknown'],
        50: [500, 'Couldn\'t get account API key'],
        51: [403, 'Bot not initialized'],
        61: [404, 'Tradeoffer not found'],
        62: [400, 'Either partnerSteamId or partnerAccountId is required'],
        63: [400, 'partnerToken should is required'],
        64: [400, 'Either itemsToGive or itemsToReceive must be not empty'],
        65: [400, 'Tradeoffer cannot be canceled'],
        66: [400, 'Tradeoffer cannot be canceled, it is not out offer'],
        67: [403, 'Tradeoffer rejected, bot account has escrow hold'],
        68: [403, 'Tradeoffer rejected, partner account has escrow hold'],

        70: [400, 'Other bot account was not initialized. Internal tradeoffer was sent, but input offer can\'t be accepted'],

        71: [400, 'Partner trade URL is no longer valid'],

        100: [500, 'Something went wrong'],
    },
    systemErrors = {
        ENOENT: [500, 'No such file or directory']
    },
    steamErrors = {
        0: [400, 'Invalid'],
        1: [400, 'OK'],
        2: [400, 'Fail'],
        3: [400, 'NoConnection'],
        5: [400, 'InvalidPassword'],
        6: [400, 'LoggedInElsewhere'],
        7: [400, 'InvalidProtocolVer'],
        8: [400, 'InvalidParam'],
        9: [400, 'FileNotFound'],
        10: [400, 'Busy'],
        11: [400, 'InvalidState'],
        12: [400, 'InvalidName'],
        13: [400, 'InvalidEmail'],
        14: [400, 'DuplicateName'],
        15: [400, 'AccessDenied'],
        16: [400, 'Timeout'],
        17: [400, 'Banned'],
        18: [400, 'AccountNotFound'],
        19: [400, 'InvalidSteamID'],
        20: [400, 'ServiceUnavailable'],
        21: [400, 'NotLoggedOn'],
        22: [400, 'Pending'],
        23: [400, 'EncryptionFailure'],
        24: [400, 'InsufficientPrivilege'],
        25: [400, 'LimitExceeded'],
        26: [400, 'Revoked'],
        27: [400, 'Expired'],
        28: [400, 'AlreadyRedeemed'],
        29: [400, 'DuplicateRequest'],
        30: [400, 'AlreadyOwned'],
        31: [400, 'IPNotFound'],
        32: [400, 'PersistFailed'],
        33: [400, 'LockingFailed'],
        34: [400, 'LogonSessionReplaced'],
        35: [400, 'ConnectFailed'],
        36: [400, 'HandshakeFailed'],
        37: [400, 'IOFailure'],
        38: [400, 'RemoteDisconnect'],
        39: [400, 'ShoppingCartNotFound'],
        40: [400, 'Blocked'],
        41: [400, 'Ignored'],
        42: [400, 'NoMatch'],
        43: [400, 'AccountDisabled'],
        44: [400, 'ServiceReadOnly'],
        45: [400, 'AccountNotFeatured'],
        46: [400, 'AdministratorOK'],
        47: [400, 'ContentVersion'],
        48: [400, 'TryAnotherCM'],
        49: [400, 'PasswordRequiredToKickSession'],
        50: [400, 'AlreadyLoggedInElsewhere'],
        51: [400, 'Suspended'],
        52: [400, 'Cancelled'],
        53: [400, 'DataCorruption'],
        54: [400, 'DiskFull'],
        55: [400, 'RemoteCallFailed'],
        56: [400, 'PasswordUnset'],
        57: [400, 'ExternalAccountUnlinked'],
        58: [400, 'PSNTicketInvalid'],
        59: [400, 'ExternalAccountAlreadyLinked'],
        60: [400, 'RemoteFileConflict'],
        61: [400, 'IllegalPassword'],
        62: [400, 'SameAsPreviousValue'],
        63: [400, 'AccountLogonDenied'],
        64: [400, 'CannotUseOldPassword'],
        65: [400, 'InvalidLoginAuthCode'],
        66: [400, 'AccountLogonDeniedNoMail'],
        67: [400, 'HardwareNotCapableOfIPT'],
        68: [400, 'IPTInitError'],
        69: [400, 'ParentalControlRestricted'],
        70: [400, 'FacebookQueryError'],
        71: [400, 'ExpiredLoginAuthCode'],
        72: [400, 'IPLoginRestrictionFailed'],
        73: [400, 'AccountLockedDown'],
        74: [400, 'AccountLogonDeniedVerifiedEmailRequired'],
        75: [400, 'NoMatchingURL'],
        76: [400, 'BadResponse'],
        77: [400, 'RequirePasswordReEntry'],
        78: [400, 'ValueOutOfRange'],
        79: [400, 'UnexpectedError'],
        80: [400, 'Disabled'],
        81: [400, 'InvalidCEGSubmission'],
        82: [400, 'RestrictedDevice'],
        83: [400, 'RegionLocked'],
        84: [400, 'RateLimitExceeded'],
        85: [400, 'AccountLoginDeniedNeedTwoFactor'],
        86: [400, 'ItemDeleted'],
        87: [400, 'AccountLoginDeniedThrottle'],
        88: [400, 'TwoFactorCodeMismatch'],
        89: [400, 'TwoFactorActivationCodeMismatch'],
        90: [400, 'AccountAssociatedToMultiplePartners'],
        91: [400, 'NotModified'],
        92: [400, 'NoMobileDevice'],
        93: [400, 'TimeNotSynced'],
        94: [400, 'SMSCodeFailed'],
        95: [400, 'AccountLimitExceeded'],
        96: [400, 'AccountActivityLimitExceeded'],
        97: [400, 'PhoneActivityLimitExceeded'],
        98: [400, 'RefundToWallet'],
        99: [400, 'EmailSendFailure'],
        100: [400, 'NotSettled'],
        101: [400, 'NeedCaptcha']
    };

//module.exports = function( code, tag, unknownErr ) {
//    var status = errors[code] ? errors[code][0] : 500,
//        err = new Error( httpStatus[status] );
//    err.status = status;
//    err.description = errors[code] ? errors[code][1] : (unknownErr ? unknownErr.message : 'Unknown error');
//    err.code = code;
//    err.tag = [];
//    if( tag ) err.tag.push( tag );
//    return err;
//};

module.exports = function( code, tag, metadata ) {
    var status = errors[code] ? errors[code][0] : 500,
        err = new Error( httpStatus[status] );
    err.status = status;
    err.description = errors[code] ? errors[code][1] : 'Unknown error';
    err.code = code;
    err.tag = [];
    if( tag ) err.tag.push( tag );
    if( metadata ) err.metadata = metadata;
    return err;
};

module.exports.steam = function( serr, tag, metadata ) {
    const errRe = /\((\d+)\)/.exec( serr.message );
    if( !errRe ) return module.exports.system( serr, tag );

    var status = steamErrors[errRe[1]] ? steamErrors[errRe[1]][0] : 500,
        err = new Error( httpStatus[status] );
    err.status = status;
    err.description = 'Steam Error: ';
    err.description += steamErrors[errRe[1]]
        ? _.snakeCase( steamErrors[errRe[1]][1] ).replace( '_', ' ' )
        : 'unknown error';
    err.code = 'steam_' + errRe[1];
    err.tag = [];
    if( tag ) err.tag.push( tag );
    if( metadata ) err.metadata = metadata;
    return err;
};

module.exports.system = function( serr, tag ) {
    console.log( serr );
    var err = new Error( 'System error' );
    err.status = 500;
    err.description = systemErrors[serr.code] ? systemErrors[serr.code][1] : serr.message;
    err.tag = [];
    err.code = serr.code;
    if( tag ) err.tag.push( tag );
    return err;
};

module.exports.tag = function( err, tag ) {
    if( !err.tag ) return module.exports.system( err, tag );
    err.tag.push( tag );
    return err;
};

module.exports.stack = module.exports.tag;