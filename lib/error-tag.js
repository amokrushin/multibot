module.exports = function( err, tag ) {
    err.tag = err.tag || [];
    err.tag.push( tag );
    return err;
};