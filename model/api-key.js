const _ = require( 'lodash' ),
    async = require( 'neo-async' ),
    fse = require( 'fs-extra' ),
    path = require( 'path' ),
    crypto = require( 'crypto' ),
    bs58 = require( 'bs58' ),
    pbkdf2Options = {
        iterations: 1000,
        keylen: 24 // bytes
    },
    configFile = path.resolve( __dirname, '../data/config.json' ),
    apiKeysPath = path.resolve( __dirname, '../data/api-keys' ),
    apiKey = {};

function getSalt( callback ) {
    async.waterfall( [
        async.apply( fse.ensureFile, configFile ),
        async.apply( fse.readJson, configFile )
    ], function( err, config ) {
        if( config && config.salt ) return callback( null, config.salt );
        const salt = crypto.randomBytes( 10 ).toString( 'hex' );
        fse.outputJson( configFile, {salt: salt}, function( err ) {
            if( err ) return callback( err );
            callback( null, salt );
        } );
    } );
}

function secretHash( apiKeySecret, salt, callback ) {
    crypto.pbkdf2( apiKeySecret, salt, pbkdf2Options.iterations, pbkdf2Options.keylen, function( err, hash ) {
        if( err ) return callback( err );
        callback( null, (new Buffer( hash, 'binary' )).toString( 'hex' ) );
    } );
}

apiKey.create = function( ip, callback ) {
    async.waterfall( [
        getSalt,
        function( salt, callback ) {
            const apiKey = {
                id: bs58.encode( crypto.randomBytes( 10 ) ),
                secret: bs58.encode( crypto.randomBytes( 10 ) ),
                password: bs58.encode( crypto.randomBytes( 32 ) )
            };
            if( typeof ip === 'string' ) apiKey.ip = ip;
            secretHash( apiKey.secret, salt, function( err, hash ) {
                if( err ) return callback( err );
                apiKey.hash = hash;
                callback( null, apiKey );
            } );
        },
        function( apiKey, callback ) {
            fse.outputJson( path.join( apiKeysPath, apiKey.id + '.json' ), _.pick( apiKey, ['id', 'hash'] ), function( err ) {
                if( err ) return callback( err );
                callback( null, apiKey );
            } );
        }
    ], function( err, apiKey ) {
        if( err ) return callback( err );
        callback( null, apiKey );
    } );
};

apiKey.getAll = function( callback ) {
    fse.readdir( apiKeysPath, function( err, files ) {
        async.map( files, function( file, callback ) {
            fse.readJson( path.join( apiKeysPath, file ), callback );
        }, callback );
    } );
};

apiKey.getById = function( id, callback ) {
    fse.readJson( path.join( apiKeysPath, id + '.json' ), callback );
};

apiKey.revoke = function( id, callback ) {
    fse.unlink( path.join( apiKeysPath, id + '.json' ), callback );
};

apiKey.validate = function( id, secret, callback ) {
    async.series( {
        apiKey: async.constant( {id: id, secret: secret} ),
        storedApiKey: async.apply( apiKey.getById, id ),
        salt: getSalt
    }, function( err, data ) {
        if( err ) return callback( err );
        secretHash( data.apiKey.secret, data.salt, function( err, hash ) {
            if( err ) return callback( err );
            callback( null, hash === data.storedApiKey.hash );
        } );
    } );
};

apiKey.validateIp = function( id, ip, callback ) {
    fse.readJson( path.join( apiKeysPath, id + '.json' ), function( err, apiKey ) {
        if( err ) return callback( err );
        callback( null, !apiKey.ip || apiKey.ip === ip );
    } );
};

apiKey.parts = function( key ) {
    const parts = key.split( '0' );
    return {
        id: parts[0],
        secret: parts[1],
        password: parts[2]
    };
};

module.exports = apiKey;