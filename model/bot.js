const _ = require( 'lodash' ),
    async = require( 'neo-async' ),
    fse = require( 'fs-extra' ),
    fsen = require( '../lib/fs-encrypt' ),
    path = require( 'path' ),
    SteamCommunity = require( 'steamcommunity' ),
    SteamTradeOffers = require( 'steam-tradeoffers' ),
    SteamTotp = require( 'steam-totp' ),
    error = require( '../lib/error' ),
    logger = require( '../lib/logger' ),
    botPath = path.resolve( __dirname, '../data/bots/' ),
    bot = {},
    steamCommunity = {},
    steamTradeOffers = {},
    incomingOffersCheckQueue = [],
    incomingOffersPriorityCheckQueue = [],
    tradeOfferState = {
        1: 'Invalid',
        2: 'This trade offer has been sent, neither party has acted on it yet.',
        3: 'The trade offer was accepted by the recipient and items were exchanged.',
        4: 'The recipient made a counter offer',
        5: 'The trade offer was not accepted before the expiration date',
        6: 'The sender cancelled the offer',
        7: 'The recipient declined the offer',
        8: 'Some of the items in the offer are no longer available (indicated by the missing flag in the output)',
        9: 'The offer hasn\'t been sent yet and is awaiting email/mobile confirmation. The offer is only visible to the sender.',
        10: 'Either party canceled the offer via email/mobile. The offer is visible to both parties, even if the sender canceled it before it was sent.',
        11: 'The trade has been placed on hold. The items involved in the trade have all been removed from both parties inventories and will be automatically delivered in the future.'
    };

function steamID3toSteamID64( steamID3 ) {
    return (new SteamCommunity.SteamID( '[U:1:' + steamID3 + ']' )).getSteamID64();
}

function assetIdMapping( itemsToReceive, itemsReceived ) {
    const groupByClass = {};

    if( !itemsToReceive || itemsToReceive.length !== itemsReceived.length ) return {};

    function transform( result, tag, items ) {
        return _.transform( items, function( result, item ) {
            const itemClass = item.appid + '_' + item.contextid + '_' + item.classid + '_' + item.instanceid;
            if( !result[itemClass] )result[itemClass] = {};
            if( !result[itemClass][tag] )result[itemClass][tag] = [];
            result[itemClass][tag].push( item.id || item.assetid );
        }, result );
    }

    transform( groupByClass, 'from', itemsToReceive );
    transform( groupByClass, 'to', itemsReceived );

    return _.transform( groupByClass, function( result, group ) {
        for( var i = 0; i < group.from.length; i++ )
        {
            result[group.from[i]] = group.to[i];
        }
    }, {} );
}

function saveCookies( id, cookies, securityPassword, callback ) {
    const cookiesFile = path.join( botPath, id.toString(), 'cookies.json' );
    fsen.outputJson( cookiesFile, cookies || {}, securityPassword, function( err ) {
        if( err ) return callback( err );
        callback( null, cookies || {} );
    } );
}

function backup2FA( id, data, securityPassword, callback ) {
    const cookiesFile = path.join( botPath, id.toString(), 'backup2fa-' + Date.now() + '.json' );
    fsen.outputJson( cookiesFile, data || {}, securityPassword, function( err ) {
        if( err ) return callback( err );
        callback( null );
    } );
}

function loadCookies( id, securityPassword, callback ) {
    const cookiesFile = path.join( botPath, id.toString(), 'cookies.json' );
    async.waterfall( [
        async.apply( fse.ensureFile, cookiesFile ),
        async.apply( fsen.readJson, cookiesFile, securityPassword )
    ], function( err, cookies ) {
        if( err ) return callback( err );
        if( !cookies ) return callback( null, {} );
        callback( null, cookies );
    } );
}

function decodeCookies( cookies ) {
    return _.fromPairs( _.map( cookies, ( cookie )=> { return cookie.split( '=' ) } ) );
}

function encodeCookies( cookies ) {
    return _.map( _.toPairs( cookies ), ( cookie )=> { return cookie.join( '=' ) } );
}

function loginError( err ) {
    switch( err.message )
    {
        case 'SteamGuard':
            return error( 20, 'td4jdy' );
        case 'SteamGuardMobile':
            return error( 26, '68lipc' );
        case 'CAPTCHA':
            return error( 21, '6acivf', err.captchaurl );
        default:
            return error.system( err, 'ju8a20' );
    }
}

function incomingOrdersPoll() {
    const pollTimeout = 5000;

    function poll() {
        var id = incomingOffersPriorityCheckQueue.shift();
        if( !id )
        {
            id = incomingOffersCheckQueue.shift();
            incomingOffersCheckQueue.push( id );
        }

        const offers = steamTradeOffers[id],
            query = {
                get_received_offers: 1,
                active_only: 1
            };

        function next() {
            setTimeout( poll, pollTimeout );
        }

        function processReceivedOffers( receivedOffers, callback ) {
            if( !_.isArray( receivedOffers ) ) return callback();

            const receivedOffersMap = _.transform( receivedOffers, function( result, tradeOffer ) {
                if( tradeOffer.trade_offer_state != 2 ) return;
                result.push( {
                    id: tradeOffer.tradeofferid,
                    fromSteamId: tradeOffer.steamid_other,
                    isBotOrder: !!~incomingOffersCheckQueue.indexOf( tradeOffer.steamid_other )
                } );
            }, [] );

            async.eachSeries( receivedOffersMap, function( tradeOffer, callback ) {
                if( tradeOffer.isBotOrder )
                {
                    logger.info( 'accept offer', tradeOffer );
                    offers.acceptOffer( {tradeOfferId: tradeOffer.id}, function( err, res ) {
                        if( err ) logger.error( err );
                        try
                        {
                            steamCommunity[id].checkConfirmations();
                        } catch( err )
                        {
                            if( err ) return logger.error( err );
                        }
                        return callback();
                    } );
                }
                else
                {
                    logger.info( 'decline offer', tradeOffer );
                    offers.declineOffer( {tradeOfferId: tradeOffer.id}, function( err, res ) {
                        if( err ) logger.error( err );
                        try
                        {
                            steamCommunity[id].checkConfirmations();
                        } catch( err )
                        {
                            if( err ) return logger.error( err );
                        }
                        return callback();
                    } );
                }
            }, callback );
        }

        if( !offers || !offers.sessionID ) return next();

        offers.getOffers( query, function( err, res ) {
            if( err ) return logger.error( err );
            if( !res.response ) return next();
            processReceivedOffers( res.response.trade_offers_received, next );
        } );
    }

    poll();
}

function updateBotsList() {
    const withCookies = [];
    const withAccount = [];
    fse.walk( botPath )
        .on( 'data', function( item ) {
            if( ~item.path.indexOf( 'cookies.json' ) ) withCookies.push( /\d{17}/.exec( item.path )[0] );
            if( ~item.path.indexOf( 'account.json' ) ) withAccount.push( /\d{17}/.exec( item.path )[0] );
        } )
        .on( 'end', function() {
            const botsList = _.intersection( withCookies, withAccount );
            logger.info( 'multibot', 'list', _.values( botsList ) );
            _.forEach( botsList, function( botId ) {
                incomingOffersCheckQueue.push( botId )
            } );
        } )
        .on( 'error', function( err ) {
            if( err.code === 'ENOENT' ) return;
            if( err ) return logger.error( err );
        } );
}

updateBotsList();
incomingOrdersPoll();

bot.init = function( id, data, securityPassword, callback ) {
    const community = new SteamCommunity(),
        loginData = _.pick( data, ['accountName', 'password', 'authCode', 'activationCode', 'captcha', 'sharedSecret', 'identitySecret'] ),
        authType = loginData.sharedSecret ? 2 : 1;

    async.waterfall( [
        async.constant( {} ),
        function( data, callback ) {
            bot.upsertAccount( id, loginData, false, securityPassword, function( err, account ) {
                if( err ) return callback( error.system( err, 'zxl13k' ) );
                if( _.isEmpty( loginData ) && !account.twoFactorReady ) return callback( error( 40, '5wcs9p' ) );
                _.assign( loginData, account );
                callback( null, data );
            } );
        },
        function( data, callback ) {
            if( !loginData.twoFactorReady ) return callback( null, data );
            loadCookies( id, securityPassword, function( err, cookies ) {
                if( err ) return callback( error.system( err, 'mca9t0' ) );
                data.cookies = cookies;
                community.setCookies( encodeCookies( cookies ) );
                callback( null, data );
            } );
        },
        function( data, callback ) {
            if( loginData.sharedSecret )
            {
                loginData.twoFactorCode = SteamTotp.generateAuthCode( loginData.sharedSecret );
            }
            community.login( _.pick( loginData, ['accountName', 'password', 'authCode', 'twoFactorCode', 'captcha'] ),
                function( err, sessionId, cookies, steamguard ) {
                    if( err ) return callback( loginError( err ) );
                    data.cookies = decodeCookies( cookies );
                    if( id != community.steamID.getSteamID64() )
                    {
                        return callback( error( 24, 't1uswo' ) );
                    }
                    community.loggedIn( function( err, loggedIn ) {
                        data.loggedIn = loggedIn;
                        if( loggedIn && authType === 2 ) loginData.twoFactorReady = true;
                        callback( null, data );
                    } );
                } );
        },
        function( data, callback ) {
            if( !loginData.twoFactorReady ) return callback( null, data );
            saveCookies( id, data.cookies, securityPassword, function( err ) {
                if( err ) return callback( error.system( err, 'gawp1s' ) );
                callback( null, data );
            } );
        },
        function( data, callback ) {
            if( loginData.twoFactorReady ) return callback( null, data );
            if( !loginData.sharedSecret || !loginData.activationCode ) return callback( null, data );
            community.finalizeTwoFactor( loginData.sharedSecret, loginData.activationCode, function( err, res ) {
                if( err ) return callback( error.tag( error( 23, err.message ), 'lzlb7p' ) );
                loginData.twoFactorReady = true;
                callback( null, data );
            } );
        },
        function( data, callback ) {
            if( loginData.twoFactorReady ) return callback( null, data );
            if( loginData.activationCode ) return callback( null, data );
            loginData.timeout = 5 * 60 * 1000 - (Date.now() - loginData.timestamp);
            if( loginData.timestamp && (loginData.timeout > 0) )
            {
                return callback( null, data );
            }
            community.enableTwoFactor( function( err, res ) {
                if( err ) return callback( error.tag( error( 23, err.message ), 's2t8sd' ) );
                if( !res || res.status !== 1 ) return callback( error( 23, 's2zg7d' ) );

                loginData.revocationCode = res.revocation_code;
                loginData.identitySecret = res.identity_secret;
                loginData.sharedSecret = res.secret_1;
                loginData.twoFactorReady = false;
                loginData.timestamp = Date.now();

                backup2FA( id, res, securityPassword, function( err ) {
                    if( err ) return callback( error.system( err, 'kzu0gt' ) );
                    callback( null, data );
                } );
            } );
        },
        function( data, callback ) {
            if( _.isEmpty( data.cookies ) ) return callback( null, data );
            community.getWebApiKey( 'localhost', function( err, apiKey ) {
                if( err ) return callback( err );
                loginData.apiKey = apiKey;
                callback( null, data );
            } );
        },
        function( data, callback ) {
            bot.upsertAccount( id, loginData, false, securityPassword, function( err, account ) {
                if( err ) return callback( err );
                _.assign( loginData, account );
                callback( null, data );
            } );
        },
        function( data, callback ) {
            if( !loginData.twoFactorReady && loginData.timestamp && loginData.timeout > 0 )
            {
                return callback( error.tag( error( 25, timeout ), 'lb6qgb' ) );
            }
            if( !loginData.twoFactorReady ) return callback( error( 22, 'pwn4ul' ) );
            callback( null, data );
        }
    ], callback );
};

bot.upsertAccount = function( id, account, tmp, securityPassword, callback ) {
    const accountFile = path.join( botPath, id, tmp ? 'account-tmp.json' : 'account.json' ),
        accountData = _.pick( account, [
            'accountName', 'password', 'sharedSecret', 'identitySecret', 'twoFactorReady', 'timestamp', 'apiKey'
        ] );
    async.waterfall( [
        async.apply( fse.ensureFile, accountFile ),
        async.apply( fsen.readJson, accountFile, securityPassword )
    ], function( err, account ) {
        if( _.isEqual( account, accountData ) ) return callback( null, accountData );
        const merged = _.assign( {}, account, accountData );
        fsen.outputJson( accountFile, merged, securityPassword, function( err ) {
            if( err ) return callback( err );
            callback( null, merged );
        } );
    } );
};

bot.getAccount = function( id, securityPassword, callback ) {
    const accountFile = path.join( botPath, id, 'account.json' );
    async.waterfall( [
        async.apply( fse.ensureFile, accountFile ),
        async.apply( fsen.readJson, accountFile, securityPassword )
    ], callback );
};


bot.status = function( id, securityPassword, callback ) {
    steamInstance( id, securityPassword, function( err ) {
        if( err ) return callback( err );
        const community = steamCommunity[id];
        community.loggedIn( callback );
    } );
};

function steamInstance( id, securityPassword, callback ) {
    function checkOnline( id, account, cookies, callback ) {
        async.retry( {times: 3, interval: 2000}, function( callback ) {
            if( !steamCommunity[id] )
            {
                steamCommunity[id] = new SteamCommunity();
                steamCommunity[id].setCookies( encodeCookies( cookies ) );
            }
            if( !steamTradeOffers[id] )
            {
                steamTradeOffers[id] = new SteamTradeOffers();
            }
            steamCommunity[id].loggedIn( function( err, loggedIn ) {
                if( err ) return callback( error.tag( err, '34rufr' ) );
                if( loggedIn )
                {
                    steamCommunity[id].startConfirmationChecker( 30000, account.identitySecret );

                    steamTradeOffers[id].setup( {
                        sessionID: cookies.sessionid,
                        webCookie: encodeCookies( cookies ),
                        APIKey: account.apiKey
                    } );

                    return callback();
                }
                else
                {
                    steamCommunity[id].login( {
                        accountName: account.accountName,
                        password: account.password,
                        twoFactorCode: SteamTotp.getAuthCode( account.sharedSecret )
                    }, function( err, sessionId, cookies ) {
                        if( err ) return callback( loginError( err ) );
                        saveCookies( id, decodeCookies( cookies ), securityPassword, function( err ) {
                            if( err ) return callback( error.system( err, 're8jit' ) );
                            return callback( null, steamCommunity[id], steamTradeOffers[id] );
                        } );
                        steamTradeOffers[id].setup( {
                            sessionID: sessionId,
                            webCookie: cookies,
                            APIKey: account.apiKey
                        } );
                    } );
                }
            } );
        }, callback );
    }

    async.parallel( {
        account: _.partial( bot.getAccount, id, securityPassword ),
        cookies: _.partial( loadCookies, id, securityPassword )
    }, function( err, data ) {
        if( err && err.message == 'Unsupported state or unable to authenticate data' ) return callback( error( 7, '4mld84' ) );
        if( err && ~err.message.indexOf( 'account.json' ) ) return callback( error( 51, 'ho19w1' ) );
        if( err && !~err.message.indexOf( 'cookies.json' ) ) return callback( error.system( err, 'lp6s1o' ) );
        if( err && ~err.message.indexOf( 'cookies.json' ) ) data.cookies = {};

        if( _.isEmpty( data.account ) ) return callback( error( 51, id ) );

        // todo: (not tested) relogin account with wrong cookies
        if( !data.cookies['steamMachineAuth' + id] ) steamCommunity[id] = null;

        if( steamCommunity[id] ) return checkOnline( id, data.account, data.cookies, callback );

        checkOnline( id, data.account, data.cookies, callback );
    } );
}

bot.twoFactorAuthCode = function( id, securityPassword, callback ) {
    bot.getAccount( id, securityPassword, function( err, account ) {
        if( err && err.message == 'Unsupported state or unable to authenticate data' ) return callback( error( 7, 'ng4ma9' ) );
        if( err && ~err.message.indexOf( 'account.json' ) ) return callback( error( 51, '8vivtu' ) );
        callback( null, SteamTotp.getAuthCode( account.sharedSecret ) );
    } );
};

bot.sendTradeoffer = function( id, data, securityPassword, callback ) {
    function mapItems( items ) {
        return _.map( _.isArray( items ) ? items : [items] || [], function( val ) {
            const item = val.split( ',' );
            return {
                assetid: item[0],
                appid: parseInt( item[1] ),
                contextid: parseInt( item[2] ),
                amount: parseInt( item[3] ) || 1
            }
        } );
    }

    const request = _.transform( data, function( result, val, key ) {
        if( ~['partnerSteamId', 'partnerAccountId'].indexOf( key ) ) result[key] = val;
        if( key === 'partnerToken' ) result['accessToken'] = val;
        if( key === 'itemsToGive' ) result['itemsFromMe'] = mapItems( val );
        if( key === 'itemsToReceive' ) result['itemsFromThem'] = mapItems( val );
        if( key === 'securityCode' ) result['message'] = val;
    }, {} );

    _.defaults( request, {
        itemsFromMe: [],
        itemsFromThem: []
    } );

    if( !request.partnerSteamId && !request.partnerAccountId ) return callback( error( 62, 'f90kdm', {request: request} ) );
    if( !request.accessToken ) return callback( error( 63, 'tc3o0y', {request: request} ) );
    if( !request.itemsFromMe.length && !request.itemsFromThem.length ) return callback( error( 64, 'abs1n5', {request: request} ) );

    const partnerSteamId = data.partnerSteamId ? data.partnerSteamId : steamID3toSteamID64( data.partnerAccountId ),
        isBotOrder = ~incomingOffersCheckQueue.indexOf( partnerSteamId );

    steamInstance( id, securityPassword, function( err ) {
        if( err ) return callback( error.tag( err, 'ho19w1' ) );

        const offers = steamTradeOffers[id];

        async.waterfall( [
            async.constant( request ),
            // check escrow hold duration
            function( request, callback ) {
                const holdDurationRequest = _.pick( request, ['partnerSteamId', 'partnerAccountId', 'accessToken'] );
                offers.getHoldDuration( holdDurationRequest, function( err, hold ) {
                    if( err && ~err.message.indexOf( 'This Trade URL is no longer valid' ) )
                        return callback( error( 71, 'hyo3d2', {request: holdDurationRequest} ) );
                    if( err ) return callback( error.system( err, 'zmo0g4' ) );
                    if( _.isEmpty( hold ) ) return callback( error( 100, 'ugsh7h', {request: holdDurationRequest} ) );
                    if( parseInt( hold.my ) > 0 ) return callback( error( 67, 'f88ep3', {request: holdDurationRequest} ) );
                    if( parseInt( hold.their ) > 0 ) return callback( error( 68, 'd67eko', {request: holdDurationRequest} ) );
                    return callback( null, request );
                } );
            },
            // send trade offer
            function( request, callback ) {
                offers.makeOffer( request, function( err, response ) {
                    if( err ) return callback( error.steam( err, 'x2rxda', {request: request} ) );
                    steamCommunity[id].checkConfirmations();
                    return callback( null, response );
                } );
            },
            // if partner is bot account then ensure it is active
            function( response, callback ) {
                if( !isBotOrder ) return callback( null, response );
                steamInstance( partnerSteamId, securityPassword, function( err ) {
                    if( err ) return callback( error( 70, '23ocqa', {partnerSteamId: partnerSteamId} ) );
                    return callback( null, response );
                } );
            }
        ], callback );
    } );
};

bot.cancelTradeoffer = function( id, tradeofferid, securityPassword, callback ) {
    steamInstance( id, securityPassword, function( err ) {
        const offers = steamTradeOffers[id];
        if( err ) return callback( err );
        offers.getOffer( {
            tradeofferid: tradeofferid
        }, function( err, res ) {
            if( err ) return callback( err );
            if( !res || !res.response || !res.response.offer ) return callback( error( 61, 'za0zp3' ) );
            if( !res.response.offer.is_our_offer ) return callback( error( 66, 'p378cy' ) );
            if( res.response.offer.trade_offer_state != '2' )
            {
                return callback( error( 65, 'y1vmes', {
                    state: res.response.offer.trade_offer_state,
                    description: tradeOfferState[res.response.offer.trade_offer_state]
                } ) );
            }
            offers.cancelOffer( {
                tradeOfferId: tradeofferid
            }, function( err, res ) {
                if( err ) return callback( err );
                callback( null, res.response );
            } );
        } );
    } );
};

bot.getTradeoffers = function( id, query, securityPassword, callback ) {
    steamInstance( id, securityPassword, function( err ) {
        const offers = steamTradeOffers[id];
        if( err ) return callback( err );
        offers.getOffers( query, function( err, res ) {
            if( err ) return callback( err );
            if( !res ) return callback( error( 100, 'sdqmf8' ) );
            callback( null, res.response );
        } );
    } );
};

bot.getTradeoffer = function( id, tradeofferid, securityPassword, callback ) {
    steamInstance( id, securityPassword, function( err ) {
        const offers = steamTradeOffers[id];
        if( err ) return callback( err );
        offers.getOffer( {
            tradeofferid: tradeofferid
        }, function( err, res ) {
            if( err ) return callback( err );
            if( !res || !res.response || !res.response.offer ) return callback( error( 61, 'za0zp3' ) );

            const offer = res.response.offer;

            if( offer.trade_offer_state != 3 ) return callback( null, offer );

            offers.getItems( {tradeId: offer.tradeid}, function( err, itemsReceived ) {
                if( err || !itemsReceived ) return callback( null, offer );
                callback( null, _.assign( offer, {
                    items_received: itemsReceived,
                    assetid_mappings: assetIdMapping( offer.items_to_receive, itemsReceived )
                } ) );
            } );
        } );
    } );
};

bot.inventory = function( id, query, securityPassword, callback ) {
    steamInstance( id, securityPassword, function( err ) {
        const offers = steamTradeOffers[id];
        if( err ) return callback( err );
        offers.loadMyInventory( query, function( err, res ) {
            if( err ) return callback( err );
            callback( null, res );
        } );
    } );
};

bot.getProfile = function( id, securityPassword, callback ) {
    steamInstance( id, securityPassword, function( err ) {
        if( err ) return callback( error.tag( err, 'h5ei50' ) );
        steamCommunity[id].getSteamUser( new SteamCommunity.SteamID( id ), function( err, profile ) {
            if( err ) return callback( error.tag( err, 'bxkqjr' ) );
            callback( null, _.omit( profile, ['_community'] ) );
        } );
    } );
};

bot.editProfile = function( id, data, securityPassword, callback ) {
    steamInstance( id, securityPassword, function( err ) {
        if( err ) return callback( error.tag( err, 'sm6ygo' ) );
        steamCommunity[id].editProfile( data, function( err ) {
            if( err ) return callback( error.tag( err, '6n27ja' ) );
            callback( null );
        } );
    } );
};

module.exports = bot;