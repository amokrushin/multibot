const _ = require( 'lodash' ),
    async = require( 'neo-async' ),
    fse = require( 'fs-extra' ),
    path = require( 'path' ),
    crypto = require( 'crypto' ),
    pem = require( 'pem' ),
    pemOptions = {
        selfSigned: true,
        keyBitsize: 1024
    },
    jwtModelsPath = path.resolve( __dirname, '../data/jwt-keys' ),
    jwt = require( 'jsonwebtoken' ),
    jwtModel = {};

function generateJwtSecret( id, callback ) {
    async.seq(
        async.apply( pem.createCertificate, pemOptions ),
        function( cert, callback ) {
            pem.getPublicKey( cert.csr, function( err, res ) {
                fse.outputFile( path.join( jwtModelsPath, id + '.pub' ), res.publicKey, function( err ) {
                    if( err ) return callback( err );
                    callback( null, cert.clientKey, res.publicKey );
                } );
            } );
        } )
    ( callback );
}

function getJwtSecret( id, callback ) {
    fse.readFile( path.join( jwtModelsPath, id + '.pub' ), {encoding: 'utf8'}, callback );
}

function encrypt( text, password ) {
    var cipher = crypto.createCipher( 'aes-256-ctr', password );
    var crypted = cipher.update( text, 'utf8', 'hex' );
    crypted += cipher.final( 'hex' );
    return crypted;
}

function decrypt( text, password ) {
    var decipher = crypto.createDecipher( 'aes-256-ctr', password );
    var dec = decipher.update( text, 'hex', 'utf8' );
    dec += decipher.final( 'utf8' );
    return dec;
}

jwtModel.token = function( id, payload, secretPayload, callback ) {
    generateJwtSecret( id, function( err, privateKey, publicKey ) {
        if( err ) return callback( err );
        const password = publicKey.match( /[\w+]{8}/ )[0],
            encPayload = secretPayload ? encrypt( JSON.stringify( secretPayload ), password ) : null;
        jwt.sign( _.assign( {}, {id: id, epl: encPayload} ), privateKey, {algorithm: 'RS256'}, function( token ) {
            callback( null, token );
        } );
    } );
};

jwtModel.verify = function( id, token, callback ) {
    getJwtSecret( id, function( err, publicKey ) {
        if( err ) return callback( err );
        jwt.verify( token, publicKey, function( err, payload ) {
            if( err ) return callback( err );
            if( payload.epl )
            {
                const password = publicKey.match( /[\w+]{8}/ )[0],
                    decPayload = decrypt( payload.epl, password );
                try
                {
                    return callback( null, _.assign( {}, payload, JSON.parse( decPayload ) ) );
                } catch( err )
                {
                    callback( null, err );
                }
            }
            callback( null, payload );
        } );
    } );
};

module.exports = jwtModel;