'use strict';

const _ = require( 'lodash' ),
    express = require( 'express' ),
    app = express(),
    path = require( 'path' ),
    fs = require( 'fs' ),
    bodyParser = require( 'body-parser' ),

    decodeJwt = require( './middlewares/decode-jwt' ),
    whitelistIp = require( './middlewares/whitelist-ip' ),
    validateJwt = require( './middlewares/validate-jwt' ),
    securityPassword = require( './middlewares/security-password' ),

    authentication = require( './controller/authentication' ),
    botsApiRouter = require( './routes/api/bots' ),

    logger = require( './lib/logger' );

app.use( bodyParser.json() );
app.use( bodyParser.urlencoded( {extended: false} ) );

app.use( function( req, res, next ) {
    req.timestamp = Date.now();
    const reqData = {};
    if( !_.isEmpty( req.query ) ) reqData.query = req.query;
    if( !_.isEmpty( req.body ) ) reqData.query = req.body;
    logger.info( 'api', req.path, 'req', _.isEmpty( reqData ) ? '' : reqData );
    next();
} );

app.all( '/api/*', [decodeJwt, whitelistIp, validateJwt, securityPassword] );
app.post( '/api/authenticate', authentication );
app.use( '/api/bots', botsApiRouter );

app.use( function( req, res ) {
    const time = Date.now() - req.timestamp;
    logger.info( 'api', req.path, 'res', 200, time + 'ms' );
} );

app.use( function( err, req, res, next ) {
    const time = Date.now() - req.timestamp,
        response = _.omit( {
            status: err.status || 500,
            message: err.message,
            description: err.description,
            code: err.code,
            tags: err.tag,
            metadata: err.metadata
        }, _.isNil );
    res.status( response.status );
    logger.error( 'api', req.path, 'res', response.status, time + 'ms', _.omit( response, ['status'] ) );
    res.json( response );
} );

module.exports = app;