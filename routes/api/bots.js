'use strict';

const express = require( 'express' ),
    router = express.Router(),
    error = require( '../../lib/error' ),
    _ = require( 'lodash' ),
    apiBotsController = require( '../../controller/api/bots' );

router.param( 'id', function( req, res, next, id ) {
    if( !/7656119\d{10}/.test( id ) ) return next( error( 10, '2c3gx5' ) );
    _.set( req, 'bot.id', id );
    next();
} );

router.param( 'tradeofferid', function( req, res, next, tradeofferid ) {
    if( !/\d+/.test( tradeofferid ) ) return next( error( 10, '2c3gx5' ) );
    _.set( req, 'bot.tradeofferid', tradeofferid );
    next();
} );

router.post( '/:id/init', apiBotsController.init );
router.get( '/:id/status', apiBotsController.status );
router.get( '/:id/twoFactorAuthCode', apiBotsController.twoFactorAuthCode );
router.get( '/:id/profile', apiBotsController.getProfile );
router.post( '/:id/profile', apiBotsController.editProfile );
router.get( '/:id/tradeoffer/:tradeofferid', apiBotsController.getTradeoffer );
router.post( '/:id/tradeoffer/:tradeofferid/cancel', apiBotsController.cancelTradeoffer );
router.post( '/:id/tradeoffer', apiBotsController.sendTradeoffer );
router.get( '/:id/tradeoffers', apiBotsController.tradeoffers );
router.get( '/:id/inventory', apiBotsController.inventory );

module.exports = router;
