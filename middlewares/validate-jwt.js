const _ = require( 'lodash' ),
    error = require( '../lib/error' ),
    jwtKeyModel = require( '../model/jwt-key' );

module.exports = function( req, res, next ) {
    if( req.path === '/api/authenticate' ) return next();
    jwtKeyModel.verify( req.jwtPayload.id, req.jwtToken, function( err, payload ) {
        if( err ) return next( error( 5, 'q9lske' ) );
        _.assign( req.jwtPayload, payload );
        next();
    } );
};