const error = require( '../lib/error' ),
    jwt = require( 'jsonwebtoken' );

module.exports = function( req, res, next ) {
    if( req.path === '/api/authenticate' ) return next();
    if( !req.header( 'Authorization' ) ) return next( error( 4, 'gcb4zm' ) );
    var token = req.header( 'Authorization' ).replace( 'JWT ', '' );
    if( !token ) next( error( 5, 'ygs84q' ) );
    req.jwtToken = token;
    var jwtPayload = jwt.decode( token );
    if( !jwtPayload || !jwtPayload.id ) next( error( 5, '7q4vg3' ) );
    req.jwtPayload = jwtPayload;
    next();
};