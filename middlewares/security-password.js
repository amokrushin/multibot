const error = require( '../lib/error' ),
    bs58 = require( 'bs58' );

module.exports = function( req, res, next ) {
    if( req.path === '/api/authenticate' ) return next();
    if( !req.jwtPayload.password ) return next( error( 6, 'z9k495' ) );
    req.securityPassword = new Buffer( bs58.decode( req.jwtPayload.password ) );
    next();
};