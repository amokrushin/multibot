const error = require( '../lib/error' ),
    apiKeyModel = require( '../model/api-key' );

module.exports = function( req, res, next ) {
    if( req.path === '/api/authenticate' ) return next();
    apiKeyModel.validateIp( req.jwtPayload.id, req.ip, function( err, isValid ) {
        if( err && err.code === 'ENOENT' ) return next( error( 5, 'wxgmi6' ) );
        if( err ) return next( error.system( err, 'm60vdf' ) );
        if( !isValid ) return next( error( 3, 'usgv77' ) );
        next();
    } );
};