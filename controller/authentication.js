const error = require( '../lib/error' ),
    tag = require( '../lib/error-tag' ),
    apiKeyModel = require( '../model/api-key' ),
    jwtKeyModel = require( '../model/jwt-key' );

module.exports = function( req, res, next ) {
    if( !req.body.apiKey ) return next( error( 1, 'o16eud' ) );
    if( !/[\w]{70,80}/.test( req.body.apiKey ) ) return next( error( 2, 'h1n0u4' ) );

    var apiKey = apiKeyModel.parts( req.body.apiKey );

    apiKeyModel.validate( apiKey.id, apiKey.secret, function( err, isValid ) {
        if( err && err.code === 'ENOENT' ) return next( error( 2, 'drhn8s' ) );
        if( err ) return next( error( 2, 'cq40ur' ) );
        if( !isValid ) return next( error( 2, 'odkrh8' ) );
        apiKeyModel.validateIp( apiKey.id, req.ip, function( err, isValid ) {
            if( !isValid ) return next( error( 3, 'usgv77' ) );
            jwtKeyModel.token( apiKey.id, {}, {password: apiKey.password}, function( err, token ) {
                if( err ) return next( error( 2, 'mpz2hi', err ) );
                res.json( {token: token} );
            } );
        } );
    } );
};