const async = require( 'neo-async' ),
    _ = require( 'lodash' ),
    error = require( '../../lib/error' ),
    botModel = require( '../../model/bot' ),
    logger = require( '../../lib/logger' ),
    apiBotsController = {};

apiBotsController.init = function( req, res, next ) {
    const data = _.pick( req.body, [
        'accountName',
        'password',
        'sharedSecret',
        'identitySecret',
        'authCode',
        'activationCode',
        'steamMachineAuth',
        'captcha'
    ] );

    botModel.init( req.bot.id, data, req.securityPassword, function( err ) {
        if( err ) return next( error.tag( err, 'awnhov' ) );
        res.json( {status: true} );
        next();
    } );
};

apiBotsController.status = function( req, res, next ) {
    botModel.status( req.bot.id, req.securityPassword, function( err, status ) {
        if( err ) return next( error.tag( err, 'l3b00s' ) );
        res.json( {status: status} );
        next();
    } );
};

apiBotsController.twoFactorAuthCode = function( req, res, next ) {
    botModel.twoFactorAuthCode( req.bot.id, req.securityPassword, function( err, code ) {
        if( err ) return next( error.tag( err, 'pztnbu' ) );
        res.json( {code: code} );
        next();
    } );
};

apiBotsController.getProfile = function( req, res, next ) {
    botModel.getProfile( req.bot.id, req.securityPassword, function( err, profile ) {
        if( err ) return next( error.tag( err, '8sbzt7' ) );
        res.status( 200 ).json( profile );
        next();
    } );
};

apiBotsController.editProfile = function( req, res, next ) {
    const data = _.pick( req.body, [
        'name',
        'realName',
        'summary',
        'country',
        'state',
        'city',
        'customURL',
        'background',
        'featuredBadge',
        'primaryGroup'
    ] );

    botModel.editProfile( req.bot.id, data, req.securityPassword, function( err ) {
        if( err ) return next( error.tag( err, '4v9k96' ) );
        res.status( 204 ).send();
        next();
    } );
};

apiBotsController.getTradeoffer = function( req, res, next ) {
    botModel.getTradeoffer( req.bot.id, req.bot.tradeofferid, req.securityPassword, function( err, tradeoffer ) {
        if( err ) return next( error.tag( err, '763xge' ) );
        res.json( tradeoffer );
        next();
    } );
};

apiBotsController.tradeoffers = function( req, res, next ) {
    const query = _.pick( _.mapKeys( req.query, _.rearg( _.snakeCase, 1, 0 ) ), [
        'get_sent_offers',
        'get_received_offers',
        'get_descriptions',
        'language',
        'active_only',
        'historical_only',
        'time_historical_cutoff'] );
    botModel.getTradeoffers( req.bot.id, query, req.securityPassword, function( err, tradeoffers ) {
        if( err ) return next( error.tag( err, '79h4qp' ) );
        res.json( tradeoffers );
        next();
    } );
};

apiBotsController.inventory = function( req, res, next ) {
    const query = _.pick( _.mapKeys( req.query, _.rearg( _.camelCase, 1, 0 ) ), [
        'appId',
        'contextId',
        'language',
        'tradableOnly'
    ] );
    botModel.inventory( req.bot.id, query, req.securityPassword, function( err, inventory ) {
        if( err ) return next( error.tag( err, '84kqda' ) );
        res.json( inventory );
        next();
    } );
};

apiBotsController.sendTradeoffer = function( req, res, next ) {
    const fixArrFields = _.transform( req.body, function( result, value, key ) {
        if( /\[\d+]/.test( key ) )
        {
            const arrKey = key.replace( /\[\d+\]/, '' );
            result[arrKey] = result[arrKey] || [];
            result[arrKey].push( value );
        }
        else
        {
            result[key] = value;
        }
    }, {} );
    const data = _.pick( _.mapKeys( fixArrFields, _.rearg( _.camelCase, 1, 0 ) ), [
        'partnerSteamId',
        'partnerAccountId',
        'partnerToken',
        'itemsToGive',
        'itemsToReceive',
        'securityCode'
    ] );
    botModel.sendTradeoffer( req.bot.id, data, req.securityPassword, function( err, response ) {
        if( err ) return next( error.tag( err, 'ziq6f0' ) );
        res.json( response );
        next();
    } );
};

apiBotsController.cancelTradeoffer = function( req, res, next ) {
    botModel.cancelTradeoffer( req.bot.id, req.bot.tradeofferid, req.securityPassword, function( err, tradeoffer ) {
        if( err ) return next( error.tag( err, '763xge' ) );
        res.json( tradeoffer );
        next();
    } );
};

module.exports = apiBotsController;