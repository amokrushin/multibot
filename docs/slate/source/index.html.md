---
title: API Reference

language_tabs:
  - http
  - javascript: node.js

toc_footers:
  - <a href='https://github.com/tripit/slate'>Documentation Powered by Slate</a>

includes:
  - errors
  - bots
  - administration
  - details

search: true
---

# Introduction

POST request bodies should have content type `application/x-www-form-urlencoded` either `application/json` and be valid JSON.

# Versioning

All API calls should be made with an `Api-Version` header which guarantees that your call is using the correct API version. The current version is **1**.

### HTTP Header
`Api-Version: 1`

# Authentication

```bash
curl -X GET -H "Authorization: JWT eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE0NTY0MjIzMDZ9.ChjdGZUZE86ZpMLB2xup-kEW6t0TDw6fcnZ6BDSrdto" http://localhost:3087/api/endpoint
```

All API calls, except of authentication endpoint, should be made with an `Authorization` HTTP header.

### HTTP Header
`Authorization: JWT AUTHORIZATION_TOKEN`

Authentication to the API is performed via API authentication endpoint. API requests without authentication will fail. You can manage your API keys in the [administration](#administration) interface. You can specify IP addresses to whitelist when creating a new API Key.

# Authentication endpoint

```bash
curl -X POST -H "Content-Type: application/x-www-form-urlencoded" -d 'apiKey=630a33c4124b62910fe6a5a753ddc1cf87d18da2' "http://localhost:3087/api/authenticate"

curl -X POST -H "Content-Type: application/json" -d '{"apiKey":"630a33c4124b62910fe6a5a753ddc1cf87d18da2"}' "http://localhost:3087/api/authenticate"
```

> The above command returns JSON structured like this:

```json
{
  "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpZCI6IjYzMGEzM2M0MTI0YjYyOTEwZmU2IiwiaWF0IjoxNDU2NTAzOTcwfQ.zu2XfC_GAU_E2_tbTFCJMfni-xI3OAcR8RDT8M8XFbE18sKG-yguxb7IjTgHMirOwjnWun-qGySn3Rl2clFGib4pL_ieHiytDbwuuCfSm6Y6MHvpAJtlkq8kTjZ7GYLULP3x5UOTHB0RfE0zP3F0NCjYhtoA1qwSATrgDno29hw"
}
```

<aside class="notice">
When you requesting a new token previous one becomes invalid.
</aside>

### HTTP Request

`POST /api/authenticate`

### Parameters

Parameter name       | Value       | Description
-------------------- | ----------- | -----------
apiKey               | string      | Your secret API key

### Response JSON

Parameter name       | Value       | Description
-------------------- | ----------- | -----------
token                | string      | JSON web token