## Internal usage of API key

### API key parts
<pre class="not-aside">
  xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
  |   <i class="ev">id</i>   ||      <i class="ev">secret</i>      ||      <i class="ev">password</i>    |
</pre>

### API key file
API key information stored in the file <span class="code">./data/api-keys/<i class="ev">id</i>.json</span>
<pre class="not-aside">
  {
    id: <i class="ev">id</i>,
    hash: <i class="ef">crypto.pbkdf2( <i class="ev">secret</i> )</i>
  }
</pre>

### JSON Web Token request flow
1. API key validation

Comparison the provided API-key <span class="code"><i class="ef">crypto.pbkdf2( <i class="ev">secret</i> )</i></span> hash with the stored in the file <span class="code">./data/api-keys/<i class="ev">id</i>.json</span>

2. Generation pairs of public and private keys to be used for JSON web token signing and validation.

Storing public key into the file <span class="code">./data/jwt-keys/<i class="ev">id</i>.pub</span>

### API endpoint request flow
Validation JSON web token with public key stored in the file <span class="code">./data/jwt-keys/<i class="ev">id</i>.pub</span>