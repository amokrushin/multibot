# Bot

## Bot account initialization

This endpoint initializes a bot account.

> Example response:

### HTTP Request

`POST /api/bots/<id>/init`

### URL Parameters

Parameter       | Description
--------------- | -----------
id              | Bot account steamID64

### Request

Parameter       | Description
--------------- | -----------
accountName     | Bot account name
password        | Bot account password
authCode        | The Steam Guard code you received in your email
activationCode  | The activation code you received in your SMS
sharedSecret    | The secret that's used for two-factor authentication
identitySecret  | The secret that's used for confirming trades

To initialize new bot you need an account protected by Steam Guard email.
You must have a phone number already linked with and verified on your account.

### 1. Account has no two factor authentication enabled

You should call this method in series

First time with `accountName` and `password` parameters.

You'll be sent an email with a Steam Guard code that you'll need to provide to the next method call.

Second time with `authCode` parameter.

On successful login will be started the process to turn on two factor authentication for your account.
You'll be sent an SMS with an activation code that you'll need to provide to the next method call.

Third time with `activationCode` parameter.

### 2. Account has two factor authentication enabled and you can provide shared_secret and identity_secret

You should call this method once and provide `accountName`, `password`, `sharedSecret` and `identitySecret` parameters.

### 3. Account has two factor authentication enabled and you can not provide shared_secret and identity_secret
Steam account can always have only one set of keys. New set can be only generated if the previous set was revoked first.

[How do I disable Steam Guard Mobile Authenticator?](https://support.steampowered.com/kb_article.php?ref=8625-WRAH-9030&l=english#disable)








## Get two-factor authentication code

```shell
curl -X GET -H "Api-Version: 2016-02-25" -H "Cache-Control: no-cache" "http://localhost:3087/api/bots/76561190000000000/twoFactorAuthCode"
```

```php
<?php

$client = new http\Client;
$request = new http\Client\Request;

$request->setRequestUrl('http://localhost:3087/api/bots/76561190000000000/twoFactorAuthCode');
$request->setRequestMethod('GET');
$request->setHeaders(array(
  'cache-control' => 'no-cache',
  'api-version' => '2016-02-25'
));

$client->enqueue($request)->send();
$response = $client->getResponse();

echo $response->getBody();
```

```javascript
var request = require( "request" );

var options = {
    method: 'GET',
    url: 'http://localhost:3087/api/bots/76561190000000000/twoFactorAuthCode',
    headers: {
        'cache-control': 'no-cache',
        'api-version': '2016-02-25'
    }
};

request( options, function( error, response, body ) {
    if( error ) throw new Error( error );

    console.log( body );
} );
```

> Example response:

```json
{
  "code": "K32VR"
}
```

This endpoint generates Steam-style 5-digit alphanumeric two-factor authentication code.
You can use it for manual login on SteamCommunity website.


### HTTP Request

`GET /api/bots/<id>/twoFactorAuthCode`

### URL Parameters

Parameter | Description
--------- | -----------
id        | Bot account steamID64








## Bot account get profile

> Example response:

```json
{
  "steamID": {
    "universe": 1,
    "type": 1,
    "instance": 1,
    "accountid": 250132139
  },
  "name": "lester.meyer48",
  "onlineState": "offline",
  "stateMessage": "Last Online 234 days ago",
  "privacyState": "public",
  "visibilityState": "3",
  "avatarHash": "5e98f3423befa157063a5b8cd671643b5639b2f0",
  "vacBanned": false,
  "tradeBanState": "None",
  "isLimitedAccount": false,
  "customURL": "",
  "memberSince": "2015-08-01T21:00:00.000Z",
  "location": "",
  "realName": "",
  "summary": "No information given.",
  "groups": null,
  "primaryGroup": null
}
```

### HTTP Request

`GET /api/bots/<id>/profile`

### URL Parameters

Parameter | Description
--------- | -----------
id        | Bot account steamID64








## Bot account edit profile

``` http
POST /api/bots/76561198210397867/profile HTTP/1.1
Host: https://multibot.mokr.org
Authorization: JWT <token>
Content-Type: application/x-www-form-urlencoded

name=BotName
```

``` javascript
var request = require("request");

var options = { method: 'POST',
  url: 'https://multibot.mokr.org/api/bots/76561198210397867/profile',
  headers: {
    'content-type': 'application/x-www-form-urlencoded',
    authorization: 'JWT <token>'
  },
  form: {
    name: 'BotName'
  }
};

request(options, function (error, response, body) {
  if (error) throw new Error(error);

  console.log(body);
});
```

This endpoint allow to update one or more parts of bot account profile.

[SteamCommunity#editProfile](https://github.com/DoctorMcKay/node-steamcommunity/wiki/SteamCommunity#editprofilesettings-callback)

> Example response:

### HTTP Request

`POST /api/bots/<id>/profile`

### URL Parameters

Parameter       | Description
--------------- | -----------
id              | Bot account steamID64

### Request

Request may containing one or more of the following properties

Parameter       | Description
--------------- | -----------
name            | Your new profile name
realName        | Your new profile "real name", or empty string to remove it
summary         | Your new profile summary
country         | A country code, like `US`, or empty string to remove it
state           | A state code, like `FL`, or empty string to remove it
city            | A numeric city code, or empty string to remove it
customURL       | Your new profile custom URL
background      | The assetid of an owned profile background which you want to equip, or empty string to remove it
featuredBadge   | The ID of your new featured badge, or empty string to remove it. Currently game badges aren't supported, only badges whose pages end in `/badge/<id>`
primaryGroup    | `SteamID`

### Response

HTTP 204 if success, error otherwise







## Status

> Example response:

```json
{
  "status": true
}
```

### HTTP Request

`GET /api/bots/<id>/status`

### URL Parameters

Parameter | Description
--------- | -----------
id        | Bot account steamID64


## Bot inventory

```
GET /api/bots/76561198210397867/inventory?appId=730&contextId=2 HTTP/1.1
Authorization: JWT <token>
```

> Example response:

```json
[
{
    "id": "3862158486",
    "classid": "937245651",
    "instanceid": "188530139",
    "amount": "1",
    "pos": 1,
    "appid": "730",
    "icon_url": "-9a81dlWLwJ2UUGcVs_nsVtzdOE...",
    "icon_url_large": "-9a81dlWLwJ2UUGcVs_nsVtzdOEdt...",
    "icon_drag_url": "",
    "name": "Desert Eagle | Bronze Deco",
    "market_hash_name": "Desert Eagle | Bronze Deco (Factory New)",
    "market_name": "Desert Eagle | Bronze Deco (Factory New)",
    "name_color": "D2D2D2",
    "background_color": "",
    "type": "Mil-Spec Grade Pistol",
    "tradable": 1,
    "marketable": 1,
    "commodity": 0,
    "market_tradable_restriction": "7",
    "descriptions": ["..."],
    "owner_descriptions": "",
    "actions": ["..."],
    "market_actions": ["..."],
    "tags": ["..."],
    "contextid": "2"
  }
]
```

### HTTP Request

`GET /api/bots/<id>/status`

### URL Parameters

Parameter | Description
--------- | -----------
id        | Bot account steamID64

### URL query Parameters

Parameter | Optional | Description
--------- | ----------- | ---
appId        | false |is the Steam AppID
contextId        | false |is the inventory context Id
language         | true |is the language for item descriptions
tradableOnly         | true |is a boolean flag that defaults to true to return tradable items only








## Get tradeoffers

```http
GET /api/bots/76561198210397867/tradeoffers?getSentOffers=1&activeOnly=1 HTTP/1.1
Host: multibot.mokr.org
Authorization: JWT <token>
```

```javascript
var request = require("request");

var options = { method: 'GET',
  url: 'https://multibot.mokr.org/api/bots/76561198210397867/tradeoffers',
  qs: { getSentOffers: '1', activeOnly: '1' },
  headers:
   { authorization: 'JWT <token>' } };

request(options, function (error, response, body) {
  if (error) throw new Error(error);

  console.log(body);
});
```

> Example response:

```json
{
  "trade_offers_sent": [
    {
      "tradeofferid": "1048771925",
      "accountid_other": 154696042,
      "message": "Security code: F8WL3",
      "expiration_time": 1457950078,
      "trade_offer_state": 2,
      "items_to_give": [
        {
          "appid": "730",
          "contextid": "2",
          "assetid": "3637765457",
          "classid": "520025252",
          "instanceid": "0",
          "amount": "1",
          "missing": false
        },
        {
          "appid": "730",
          "contextid": "2",
          "assetid": "3637766006",
          "classid": "520025252",
          "instanceid": "0",
          "amount": "1",
          "missing": false
        }
      ],
      "is_our_offer": true,
      "time_created": 1456740478,
      "time_updated": 1456740492,
      "from_real_time_trade": false,
      "escrow_end_date": 0,
      "confirmation_method": 2,
      "steamid_other": "76561198114961770"
    }
  ]
}
```

[IEconService#GetTradeOffers](https://developer.valvesoftware.com/wiki/Steam_Web_API/IEconService#GetTradeOffers_.28v1.29)

### HTTP Request

`GET /api/bots/<id>/tradeoffers>`

### URL query Parameters
<aside class="notice">
It accepts both CamelCase so and snake_case parameter names
</aside>
Parameter               | Description
----------------------- | -----------
getSentOffers         | return the list of offers you've sent to other people.
getReceivedOffers     | return the list of offers you've received from other people.
getDescriptions        | return item display information for any items included in the returned offers.
language                | needed if get_descriptions is set, the language to use for item descriptions.
activeOnly             | return only trade offers in an active state (offers that haven't been accepted yet), or any offers that have had their state change since time_historical_cutoff.
historicalOnly         | return trade offers that are not in an active state.
timeHistoricalCutoff  | a unix time value. when active_only is set, inactive offers will be returned if their state was updated since this time. Useful to get delta updates on what has changed. WARNING: If not passed, this will default to the time your account last viewed the trade offers page. To avoid this behavior use a very low or very high date.








## Get tradeoffer

```http
GET /api/bots/76561198210397867/tradeoffer/1048771925 HTTP/1.1
Host: multibot.mokr.org
Authorization: JWT <token>
```

```javascript
var request = require("request");

var options = { method: 'GET',
  url: 'https://multibot.mokr.org/api/bots/76561198210397867/tradeoffer/1048771925',
  headers:
   { authorization: 'JWT <token>' } };

request(options, function (error, response, body) {
  if (error) throw new Error(error);

  console.log(body);
});
```
> Example response:

```json
{
  "tradeofferid": "1050628727",
  "accountid_other": 250132139,
  "message": "Security code: YEEA5",
  "expiration_time": 1458021304,
  "trade_offer_state": 3,
  "items_to_give": [
    {
      "appid": "730",
      "contextid": "2",
      "assetid": "5291829986",
      "classid": "520025252",
      "instanceid": "0",
      "amount": "1",
      "missing": true
    },
    {
      "appid": "730",
      "contextid": "2",
      "assetid": "5291829970",
      "classid": "520025252",
      "instanceid": "0",
      "amount": "1",
      "missing": true
    }
  ],
  "items_to_receive": [
    {
      "appid": "730",
      "contextid": "2",
      "assetid": "3862158486",
      "classid": "937245651",
      "instanceid": "188530139",
      "amount": "1",
      "missing": true
    },
    {
      "appid": "730",
      "contextid": "2",
      "assetid": "3859547167",
      "classid": "1309991125",
      "instanceid": "188530139",
      "amount": "1",
      "missing": true
    },
    {
      "appid": "730",
      "contextid": "2",
      "assetid": "3637828401",
      "classid": "520025252",
      "instanceid": "0",
      "amount": "1",
      "missing": true
    },
    {
      "appid": "730",
      "contextid": "2",
      "assetid": "3637797617",
      "classid": "520025252",
      "instanceid": "0",
      "amount": "1",
      "missing": true
    },
    {
      "appid": "730",
      "contextid": "2",
      "assetid": "3637788152",
      "classid": "520025252",
      "instanceid": "0",
      "amount": "1",
      "missing": true
    }
  ],
  "is_our_offer": false,
  "time_created": 1456811704,
  "time_updated": 1456811731,
  "tradeid": "511527349963127696",
  "from_real_time_trade": false,
  "escrow_end_date": 0,
  "confirmation_method": 2,
  "steamid_other": "76561198210397867",
  "items_received": [
    {
      "id": "5291853448",
      "owner": "76561198144641123",
      "classid": "520025252",
      "instanceid": "0",
      "icon_url": "-9a81dlWLwJ2UUGcVs_nsVtzdOEdtWwKGZZLQHTxDZ7I56KU0Zwwo4NUX4oFJZEHLbXU5A1PIYQNqhpOSV-fRPasw8rsUFJ5KBFZv668FFMu1aPMI24auITjxteJwPXxY72AkGgIvZAniLjHpon2jlbl-kpvNjz3JJjVLFG9rl1YLQ",
      "icon_drag_url": "",
      "name": "Operation Breakout Weapon Case",
      "market_hash_name": "Operation Breakout Weapon Case",
      "market_name": "Operation Breakout Weapon Case",
      "name_color": "D2D2D2",
      "background_color": "",
      "type": "Base Grade Container",
      "tradable": 1,
      "marketable": 1,
      "commodity": 1,
      "market_tradable_restriction": "7",
      "descriptions": ["..."],
      "owner_descriptions": "",
      "tags": ["..."],
      "pos": 1,
      "appid": 730,
      "contextid": 2,
      "amount": 1,
      "is_stackable": false
    },
    "..."
  ],
  "assetid_mappings": {
    "3637788152": "5291853472",
    "3637797617": "5291853458",
    "3637828401": "5291853448",
    "3859547167": "5291853482",
    "3862158486": "5291853499"
  }
}
```

<aside class="notice">
<b>items_received</b> and <b>assetid_mappings</b> are an additional fields which are not present in the Steam Web API GetTradeOffer method response
</aside>

### HTTP Request

`GET /api/bots/<id>/tradeoffer/<tradeofferid>`

### URL Parameters

Parameter       | Description
--------------- | -----------
id              | Bot account steamID64
tradeofferid    | The trade offer's unique numeric ID, represented as a string







## Send tradeoffer

```javascript
var request = require("request");

var options = { method: 'POST',
  url: 'https://multibot.mokr.org/api/bots/76561198210397867/tradeoffer',
  headers:
   { 'content-type': 'application/x-www-form-urlencoded',
     authorization: 'JWT <token>' },
  form:
   { partnerAccountId: '154696042',
     partnerToken: 'zzl8sVXp',
     itemsToGive: [ '3637765457,730,2,1', '3637766006,730,2,1' ],
     securityCode: 'Security code: F8WL3' } };

request(options, function (error, response, body) {
  if (error) throw new Error(error);

  console.log(body);
});
```

```http
POST /api/bots/76561198210397867/tradeoffer HTTP/1.1
Host: multibot.mokr.org
Authorization: JWT <token>
Content-Type: application/x-www-form-urlencoded

partnerAccountId=154696042&partnerToken=zzl8sVXp&itemsToGive=3637765457%2C730%2C2%2C1&itemsToGive=3637766006%2C730%2C2%2C1&securityCode=Security+code%3A+F8WL3
```

> Example response:

```json
{
  "tradeofferid": "1048771925",
  "needs_mobile_confirmation": true,
  "needs_email_confirmation": false,
  "email_domain": "gmail.com"
}
```

> Example error response for untradable item:

```json
{
  "status": 400,
  "message": "Bad Request",
  "description": "Steam Error: revoked",
  "code": "steam_26",
  "tags": [
    "x2rxda",
    "ziq6f0"
  ],
  "metadata": {
    "request": {
      "partnerAccountId": "184375395",
      "accessToken": "O1Ht8yBr",
      "itemsFromThem": [
        {
          "assetid": "5287786821",
          "appid": 730,
          "contextid": 2,
          "amount": 1
        }
      ],
      "message": "Security code: 12345",
      "itemsFromMe": []
    }
  }
}
```

### HTTP Request

`POST /api/bots/<id>/tradeoffer`

### URL Parameters

Parameter | Description
--------- | -----------
id        | Bot account steamID64


### Item

Each item should be represented as a comma delimited string `<assetid>,<appid>,<contextid>,<amount>`

Field       | Comment
----------- | -----------
assetid     | The item's asset ID within its context (the property can also be named id)
appid       | The ID of the app to which the item belongs
contextid   | The ID of the context within the app to which the item belongs
amount      | Default 1, if the item is stackable, this is how much of the stack will be added

Trade URL contains `<partnerAccountId>` and `<partnerToken>`

<pre class="not-aside">
https://steamcommunity.com/tradeoffer/new/?partner=<i class="ev">&lt;partnerAccountId&gt;</i>&token=<i class="ev">&lt;partnerToken&gt;</i>
</pre>

### Request

Parameter           | Description
------------------- | -----------
partnerAccountId    | A Steam ID in the steamID3 format
partnerSteamId      | A Steam ID in the steamID64 format, can be used instead of partnerAccountId
partnerToken        | Token from trade URL
itemsToGive[]       | An array of items to be given from bot account, can be omitted if empty
itemsToReceive[]    | An array of items to be given from the other account and received by bot, can be omitted if empty
securityCode        | Security code will be shown inside the trade offer, optional








## Cancel tradeoffer

```http
POST /api/bots/76561198210397867/tradeoffer/1048771925/cancel HTTP/1.1
Host: multibot.mokr.org
Authorization: JWT <token>
```

```javascript
var request = require("request");

var options = { method: 'POST',
  url: 'https://multibot.mokr.org/api/bots/76561198210397867/tradeoffer/1048771925/cancel',
  headers:
   { authorization: 'JWT <token>' } };

request(options, function (error, response, body) {
  if (error) throw new Error(error);

  console.log(body);
});
```

> Example response:

```json
{}
```

### HTTP Request

`GET /api/bots/<id>/tradeoffer/<tradeofferid>/cancel`

### URL Parameters

Parameter       | Description
--------------- | -----------
id              | Bot account steamID64
tradeofferid    | The trade offer's unique numeric ID, represented as a string