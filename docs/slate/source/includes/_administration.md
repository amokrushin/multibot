# Administration

## API keys

The API key consists of two parts represented as a string length of 40 characters. The first 20 characters is an API key ID, and the following 20 characters part is an API key secret. The API key secret do not stored, only its hash.

### Command-Line Interface

<pre class="highlight bash not-aside">
  node .\bin\api-key --help

    Usage: api-key [options]

    Options:

      -h, --help          output usage information
      -V, --version       output the version number
      -l, --list          API keys listing
      -c, --create [ip]   Create new API key, optional whitelist IP-address
      -r, --revoke <key>  Revoke API key
</pre>

### Examples

<pre class="highlight bash not-aside">
  node .\bin\api-key -c
    Your new API key is: bc6f759dd88354d57f37a60effded24ddbc4478a

  node .\bin\api-key -c 203.0.113.1
    Your new API key is: ac5d3268acf9b2ca2eb4d257a2529c166f599b47 | restricted to an IP-address: 203.0.113.1

  node .\bin\api-key -l
    Registered API keys
    API key ID              | Restricted to an IP-address
    -----------------------------------------------------
    ac5d3268acf9b2ca2eb4... | 203.0.113.1
    bc6f759dd88354d57f37... |

  node .\bin\api-key -r ac5d3268acf9b2ca2eb4
    API key ac5d3268acf9b2ca2eb4... was revoked

  node .\bin\api-key -r bc6f759dd88354d57f37a60effded24ddbc4478a
    API key bc6f759dd88354d57f37... was revoked
</pre>

## Starting the server

### Command-Line Interface

<pre class="highlight bash not-aside">
  node .\bin\api --help

  Usage: api [options]

  Options:

    -h, --help         output usage information
    -H, --host <host>  specify the host [127.0.0.1]
    -p, --port <port>  specify the port [3087]

</pre>

### Examples

<pre class="highlight bash not-aside">

  node .\bin\api
  [2016-02-26 09:21:21.227 7256] Listening on 127.0.0.1:3087

  node .\bin\api -H 0.0.0.0 -p 3088
  [2016-02-26 09:22:15.642 8252] Listening on 0.0.0.0:3088

</pre>
