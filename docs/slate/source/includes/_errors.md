# Errors

Error response is self-descriptive, so no information here.

```json
{
  "status": 403,
  "message": "Forbidden",
  "description": "Bot not initialized",
  "code": 51,
  "tags": [
    "ho19w1",
    "763xge"
  ]
}
```
```json
{
  "status": 404,
  "message": "Not found",
  "description": "Tradeoffer not found",
  "code": 61,
  "tags": [
    "za0zp3",
    "763xge"
  ]
}
```
```json
{
  "status": 400,
  "message": "Bad Request",
  "description": "Tradeoffer cannot be canceled, it is not out offer",
  "code": 66,
  "tags": [
    "p378cy",
    "763xge"
  ]
}
```