# Multibot

## Installation

    git clone https://amokrushin@bitbucket.org/amokrushin/multibot.git
    cd multibot/

    npm i


## Documentation

[multibot-api-doc.mokr.org](http://multibot-api-doc.mokr.org/)

### [Slate](https://github.com/tripit/slate) API docs

    cd ./docs/slate

Initialize and start Slate

    bundle install

Run local HTTP server

    bundle exec middleman server

Build static HTML

    bundle exec middleman build --clean
